# Changelog

See [Keep a Changelog](http://keepachangelog.com/).


## [2.0.0] - 2022-09-18
### Added
- Suporte a novos cargos

## [1.0.0] - 2020-11-08
### Added
- First release!
